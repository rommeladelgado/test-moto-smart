export  const baseUrl = './src/views'
export  const baseShared = './src/shared'

module.exports = {
    baseUrl,
    baseShared
}