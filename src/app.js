const baseUrl = './src/views'
const app = angular.module('appRoot', ['ngRoute', 'uploadModule']).factory('navigationService', function ($location) {
    return {
        redirectTo: function (path) {
            $location.path(path);
            $location.url($location.path());
        }
    }
}).directive('fileInput', function() {
    return {
        scope: {
            fileInput: '=ngModel'
        },
        link: function(scope, element) {
            element.on('change', function(event) {
                var files = event.target.files;
                scope.fileInput = files[0];
                scope.$apply();
            });
        }
    };
})

app.config(['$routeProvider', function (provider) {
    provider.when('/confirmation-message', {
        templateUrl: `${baseUrl}/confirmation-message/confirmation-message.html`,
        controller: 'confirmationMessageCtrl'
    })
        .when('/category', {
            templateUrl: `${baseUrl}/category/category.html`,
            controller: 'categoryCtrl'
        })
        .when('/title-publication', {
            templateUrl: `${baseUrl}/title-publication/title-publication.html`,
            controller: 'titlePublicationCtrl'
        })
        .when('/description-publication', {
            templateUrl: `${baseUrl}/description-publication/description-publication.html`,
            controller: 'titlePublicationCtrl'
        })
        .otherwise({
        redirectTo: '/confirmation-message'
    });
}])