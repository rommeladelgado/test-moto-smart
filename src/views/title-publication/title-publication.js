app.controller('titlePublicationCtrl', ['$scope', 'navigationService', function($scope, navigationService) {
    const controller = $scope;

    controller.title = ''
    controller.onBack = function () {
        navigationService.redirectTo('/category')
    }
    controller.onContinue = function () {
        navigationService.redirectTo('/description-publication')
    }

    controller.onChangeTitle = function (title) {
        controller.title = title;
    }
}]);