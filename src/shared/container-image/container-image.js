app.component('appImg', {
    bindings: {
        src: '<',
        remove: '&'
    },
    templateUrl: `./src/shared/container-image/container-image.html`,
    controller: function () {
        const ctrl = this
        ctrl.handleChange = function () {
            ctrl.onChange({value: ctrl.value});
        }
    }
})