app.controller('categoryCtrl', ['$scope', 'navigationService', function($scope, navigationService) {
    const controller = $scope;
    controller.selectedIndex = -1
    controller.list = [
        {
            title: 'Viajes y turismo'
        },
        {
            title: 'Moto lujos'
        },
        {
            title: 'Compra venta'
        },
        {
            title: 'Concesionarios',
        },
        {
            title: 'Gruas',
        },
        {
            title: 'Lavaderos',
        },
        {
            title: 'Llantas',
        },
        {
            title: 'Accesorios'
        },
        {
            title: 'Repuestos',
        },
        {
            title: 'Tecnomecanica'
        }
    ]

    controller.list = controller.list.map((e, index) => ({ ...e, name: `category_0${index}`}))

    controller.onBack = function () {
        navigationService.redirectTo('/confirmation-message')
    }
    controller.onContinue = function () {

        if (controller.selectedIndex !== -1) {
            navigationService.redirectTo('/title-publication')
        }

    }
    controller.onSelected = function (index) {
        controller.selectedIndex = index
    }

}]);