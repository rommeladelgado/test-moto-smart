app.component('appInputText', {
    bindings: {
        value: '<',
        placeholder: '<',
        maxLength: '<',
        onChange: '&'
    },
    templateUrl: `./src/shared/input-text/input-text.html`,
    controller: function () {
        const ctrl = this
        ctrl.handleChange = function () {
            ctrl.onChange({value: ctrl.value});
        }
    }
})