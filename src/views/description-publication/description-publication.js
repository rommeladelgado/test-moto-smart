app.controller('descriptionPublicationCtrl', ['$scope', 'navigationService', function($scope, navigationService) {
    const controller = $scope;

    controller.description = ''
    controller.onBack = function () {
        navigationService.redirectTo('/title-publication')
    }
    controller.onContinue = function () {
        navigationService.redirectTo('/upload/images')
    }

    controller.onChangeDescription = function (description) {
        controller.description = description;
    }
}]);