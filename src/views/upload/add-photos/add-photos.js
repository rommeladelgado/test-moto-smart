app.controller('uploadAddPhotosCtrl', ['$scope', 'navigationService', function($scope, navigationService) {
    const controller = $scope;


    controller.listUploadFiles = [];
    controller.selectedFile = null;
    controller.$watch('selectedFile', function(newFile, oldFile) {
        if (newFile !== oldFile) {

            const reader = new FileReader();

            console.log(typeof newFile)
            reader.onload = function(e) {
                controller.$apply(function() {
                    console.log(e.target.result)
                    controller.fileInput.push({
                        name: newFile.name,
                        imageSrc: e.target.result
                    });
                });
            };
            reader.readAsDataURL(newFile);
            controller.listUploadFiles = [...controller.listUploadFiles, newFile]
            controller.selectedFile = null;
        }
    });

    controller.removeImage = function (index) {
        controller.listUploadFiles = controller.listUploadFiles.filter((_, i) => index !== i)
        controller.selectedFile = null;
    }

    controller.getFileUrl = function(file) {
        return URL.createObjectURL(file);
    };
    controller.onContinue = function () {
        navigationService.redirectTo('/')
    }
    controller.onBack = function () {
        navigationService.redirectTo('/upload/images')
    }

    controller.openInputFile = function () {
        const inputFile = document.getElementById('open-file')
        inputFile.click()
    }


}]);