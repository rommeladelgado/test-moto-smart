app.component('appCardCategory', {
    bindings: {
        imageUrl: '<',
        title: '<',
        selected: '<',
        onSelected: '&'
    },
    templateUrl: `./src/shared/card-category/card-category.html`,
    controller: function () {

    }
})