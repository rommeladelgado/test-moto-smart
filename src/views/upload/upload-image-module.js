const baseUploadUrl = './src/views/upload'
const uploadModule = angular.module('uploadModule', ['ngRoute'])

uploadModule.config(['$routeProvider', function (provider) {
    provider.when('/upload/images', {
        templateUrl: `${baseUploadUrl}/info/info.html`,
        controller: 'uploadInformationCtrl'
    })
        .when('/upload/images/add', {
        templateUrl: `${baseUploadUrl}/add-photos/add-photos.html`,
        controller: 'uploadAddPhotosCtrl'
    })
}])