app.component('appMainBarContinue', {
    bindings: {
        active: '<',
        title: '<',
        onBack: '&',
        onContinue: '&',
        showButtonContinue: '<'
    },
    templateUrl: `./src/shared/main-bar-continue/main-bar-continue.html`,
    controller: function () {

    }
})