app.controller('uploadInformationCtrl', ['$scope', 'navigationService', function($scope, navigationService) {
    const controller = $scope;


    controller.onContinue = function () {
        navigationService.redirectTo('/upload/images/add')
    }
    controller.onBack = function () {
        navigationService.redirectTo('/description-publication')
    }


}]);